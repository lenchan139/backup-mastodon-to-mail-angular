import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainpageComponent } from './pages/mainpage/mainpage.component';
import { RedirectPageComponent } from './pages/redirect-page/redirect-page.component';


const routes: Routes = [
  {
    path:'redirect/:domain',
    component: RedirectPageComponent
  },
  {
    path: '',
    component: MainpageComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
