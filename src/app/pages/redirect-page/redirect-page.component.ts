import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularFireFunctions } from '@angular/fire/functions';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-redirect-page',
  templateUrl: './redirect-page.component.html',
  styleUrls: ['./redirect-page.component.less']
})
export class RedirectPageComponent implements OnInit {
  domain = ''
  code = ''
  isLoading = false
  email = ''
  buttonDisabled : ''|'disabled' = ''
  constructor(
    private activatedRoute: ActivatedRoute,
    public afFunctions: AngularFireFunctions
  ) { }

  ngOnInit() {

    this.activatedRoute.queryParams.subscribe(async params => {
      this.domain = this.activatedRoute.snapshot.paramMap.get('domain');
      this.code = params['code'];
    });
  }

  async onSubmit() {
    this.buttonDisabled = 'disabled'
    if (this.domain && this.code && this.email && this.email.includes('@') && this.email.includes('.')) {
      let r = await this.afFunctions.httpsCallable('registerAccount')({
        domain: this.domain,
        thisDomain: environment.thisDomain,
        code: this.code,
        email: this.email
      }).toPromise()
      console.log(r)
      if(r){
        alert('success')
        location.replace('/')
      }else{
        alert('fail')
        location.replace('/')
      }
    }
    else {
      alert('Invalid Email')
    }
    this.buttonDisabled = ''
  }
}
