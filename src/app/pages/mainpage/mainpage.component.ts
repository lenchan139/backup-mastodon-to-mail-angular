import { Component, OnInit } from '@angular/core';
import { postData } from 'src/app/class/common_objects';
import { environment } from 'src/environments/environment';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireFunctions } from '@angular/fire/functions';

@Component({
  selector: 'app-mainpage',
  templateUrl: './mainpage.component.html',
  styleUrls: ['./mainpage.component.less']
})
export class MainpageComponent implements OnInit {
  domain = ''
  buttonDisabled : ''|'disabled' = ''
  constructor(
    public firestore :AngularFirestore,
    public afFunctions : AngularFireFunctions
  ) { }

  ngOnInit() {
  }
  async onSubmit(){
    this.buttonDisabled = 'disabled'
    if(this.domain && this.domain.includes('.')){
      
      let thisDomain = environment.thisDomain
      let result = await this.afFunctions.httpsCallable('getApps')({
        domain: this.domain,
        thisDomain: thisDomain
      }).toPromise()
      console.log(result)
      if(result && result.client_id && result.client_secret && result.redirect_uri){
        let rURL = `https://${this.domain}/oauth/authorize?response_type=code&client_id=${result.client_id}&client_secret=${result.client_secret}&redirect_uri=${encodeURI(result.redirect_uri)}&scope=read follow`
        console.log(rURL)
        open(rURL)
      }

    }else{
      alert('Enter your domain')
    }
    this.buttonDisabled = ''
  }

}
