// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  thisDomain: "http://localhost:4200",
  firebaseConfig: {
    apiKey: "AIzaSyDQ844QJuz2XSDtErpdMCaCJdc-6Gji8K8",
    authDomain: "backup-mastodon-to-mail.firebaseapp.com",
    databaseURL: "https://backup-mastodon-to-mail.firebaseio.com",
    projectId: "backup-mastodon-to-mail",
    storageBucket: "backup-mastodon-to-mail.appspot.com",
    messagingSenderId: "526037034285",
    appId: "1:526037034285:web:21b7d7145a27bd9ad39fa4",
    measurementId: "G-YXW3LHFTC8"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
